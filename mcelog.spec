#needsrootforbuild
%define last_tar_version 198
%define mce_inject_ver 2013.01.19
%define aer_inject_ver 2010.03.10

Name:	mcelog
Version:	198
Release:	3
Epoch:		3
Summary:	Linux kernel machine check handling middleware
License:	GPLv2
URL:		https://github.com/andikleen/mcelog
Source0:	https://github.com/andikleen/%{name}/archive/mcelog-%{last_tar_version}.tar.gz
Source1:	mce-inject-%{mce_inject_ver}.tar.bz2
Source2:	aer-inject-%{aer_inject_ver}.tar.bz2
Source3:	mcelog.conf

ExclusiveArch:  i686 x86_64 loongarch64 riscv64
BuildRequires:	bison flex systemd gcc psmisc
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
mcelog logs and accounts machine checks (in particular memory, IO, and
CPU hardware errors) on modern x86 Linux systems.
mce-inject allows to inject machine check errors on the software level
into a running Linux kernel. This is intended for validation of the
kernel machine check handler.
aer-inject allows to inject PCIE AER errors on the software level into
a running Linux kernel. This is intended for validation of the PCIE
driver error recovery handler and PCIE AER core handler.

%prep
%setup -q -n %{name}-%{last_tar_version} -a 1 -a 2 

%build
make CFLAGS="$RPM_OPT_FLAGS  -Wl,-z,relro,-z,now -fpie" LDFLAGS="-Wl,-z,relro,-z,now -fpie -pie"
make CFLAGS="$RPM_OPT_FLAGS -g" -C mce-inject-%{mce_inject_ver}
make CFLAGS="$RPM_OPT_FLAGS -g -D_GNU_SOURCE" -C aer-inject-%{aer_inject_ver}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_unitdir}
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/triggers
DESTDIR=$RPM_BUILD_ROOT make install
# Replace mcelog.conf in mcelog tarball
install -p -m644 %{SOURCE3} $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/mcelog.conf
mv $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/*-trigger $RPM_BUILD_ROOT/%{_sysconfdir}/mcelog/triggers
install -p -m644 mcelog.service $RPM_BUILD_ROOT%{_unitdir}/mcelog.service
destdir=$RPM_BUILD_ROOT make -C mce-inject-%{mce_inject_ver} install
install -p -m755 aer-inject-%{aer_inject_ver}/aer-inject $RPM_BUILD_ROOT/%{_sbindir}/aer-inject

%check
%if "%{_gpg_name}" == "private OBS"
export PATH="$PATH:$RPM_BUILD_ROOT%{_sbindir}"
make test
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%post
%systemd_post mcelog.service

%preun
%systemd_preun mcelog.service

%postun
%systemd_postun_with_restart mcelog.service

%files
%doc README.md
%license LICENSE
%{_sbindir}/mce-inject
%{_sbindir}/aer-inject
%{_sbindir}/mcelog
%dir %{_sysconfdir}/mcelog
%{_sysconfdir}/mcelog/triggers
%config(noreplace) %{_sysconfdir}/mcelog/mcelog.conf
%{_unitdir}/mcelog.service
%attr(0644,root,root) %{_mandir}/*/*

%changelog
* Sun Jan 12 2025 Dingli Zhang <dingli@iscas.ac.cn> - 3:198-3
- add support for riscv64

* Tue Oct 29 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 3:198-2
- add loongarch64 support for mcelog

* Wed Aug  7 2024 dillon chen <dillon.chen@gmail.com> - 3:198-1
- upgrade to 3:198-1

* Mon Mar 25 2024 zhangruifang <zhangruifang@h-partners.com> - 3:196-2
- backport patches from upstream
- enable check only during obs build

* Mon Jan 29 2024 zhuofeng <zhuofeng2@huawei.com> - 3:196-1
- upgrade to 3:196-1

* Thu Jul 20 2023 sunhui <sunhui@kylinos.cn> - 3:194-1
- Update package to version 3:194-1

* Thu Jan 19 2023 Lv Ying <lvying6@huawei.com> - 3:191-1
- upgrade to 3:191-1

* Tue Nov 22 2022 shixuantong <shixuantong1@huawei.com> - 3:179-3
- enable check

* Sat Oct 29 2022 dongyuzhen <dongyuzhen@h-partners.com> - 179-2
- Rebuild for next release

* Wed Dec 29 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 179-1
- update version to 179

* Fri May 28 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 175-2
- The "cc:command not found" error message is displayed during compilation.Therefore,add buildrequires gcc.

* Sat Jan 23 2021 zoulin <zoulin13@huawei.com> - 175-1
- update to 175

* Tue Sep 1 2020 zhangxingliang <zhangxingliang3@huawei.com> - 170-2
- modify source url

* Mon Jul 27 2020 zhangxingliang <zhangxingliang3@huawei.com> - 170-1
- update to 170

* Thu Feb 27 2020 openEuler Buildteam <buildteam@openeuler.org> - 168-1
- Package init
